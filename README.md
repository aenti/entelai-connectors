# entelai-connectors #

entelai-connectors contains the modules used to connect to entelai services

### Usage ###

* Install using pip: `pip install git+https://bitbucket.org/aenti/entelai-connectors.git@v1.0#egg=entelaiconn`
* import connector: `from entelaiconn.<connector> import <Connector> 

### Dev & Testing ###
For developoing and running the tests extra libs should be installed. 

* Clone repo
* run `pip install -e ".[dev]"` 

mypy, flake and converage plugins are included and should be run like this:

* `pytest --flake8 --mypy --cov tests/test_memory_connector.py ` 
