from setuptools import setup, find_packages

setup(
    name="entelaiconn",
    version="1.0.0",
    url="https://bitbucket.org/aenti/entelai-connectors",
    author="Francisco Dorr",
    author_email="fdorr@entelai.com",
    description="Connectors for services.",
    packages=find_packages(),
    install_requires=[
        "requests == 2.25.1",
        "python-dotenv==0.13.0",
    ],
    extras_require={
        "dev": ["pytest", "requests_mock", "pytest-mypy", "pytest-flake8", "pytest-cov"]
    },
)
