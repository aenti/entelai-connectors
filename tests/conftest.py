import pytest
import requests_mock
from entelaiconn.memory_connector import MemoryConnector
import os


@pytest.fixture(autouse=True)
def patched_os_env(monkeypatch):
    """Mock environment vars to use the same in each test

    Mocked vars:
    - MEM_BASE_URL
    - MEM_USER
    - MEM_PASSWORD

    Args:
        monkeypatch ([type]): monkeypatch fixture
    """

    base_url = "mock://test_memoria.com"
    username = "user"
    password = "password"

    monkeypatch.setenv("MEM_BASE_URL", base_url)
    monkeypatch.setenv("MEM_USER", username)
    monkeypatch.setenv("MEM_PASSWORD", password)


@pytest.fixture
def memoria_connector():
    memoria_connector = MemoryConnector(
        os.getenv("MEM_BASE_URL"), os.getenv("MEM_USER"), os.getenv("MEM_PASSWORD")
    )
    return memoria_connector


@pytest.fixture
def request_with_token():
    """Fixture that returns a request mock, with token urls faked

    Yields:
        Mock: returns a request_mock, with token url faked
    """
    with requests_mock.Mocker() as request_mock:
        uri = "mock://test_memoria.com/api-token-auth/"
        request_mock.post(uri, json={"token": "test-token"})
        yield request_mock
