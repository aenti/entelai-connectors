import os
import requests_mock
from entelaiconn.schemas import SessionType, FinishStatus


def test_memoria_initialize_enviorenmente_vars(memoria_connector):
    assert memoria_connector.base_url == os.getenv("MEM_BASE_URL")


def test_memoria_headers_calls_request(memoria_connector):
    """Test that get_auth_headers return a token in a dict

    Given: a memoria_conector and some enviorenment configurations
    When: the *_get_auth_headers* functions is called
    Then: return a dict with token autorization

    Args:
        memoria_connector (Memoria): fixture with a MemoriaClass configured with no real url
    """
    uri = "mock://test_memoria.com/api-token-auth/"
    with requests_mock.Mocker() as request_mock:
        request_mock.post(uri, json={"token": "test-token"})
        header = memoria_connector._get_auth_headers()
        assert header["Authorization"] == "Token test-token"


def test_get_report_session_call_endpoint(memoria_connector):
    """Test that *get_report_session* calls specified endpoints

    Given: a memoria_conector and some enviorenment configurations
    When: *get_report_session* function is called
    Then: the spected urls are called with specified parameters

    Args:
        memoria_connector (Memoria): fixture with a MemoriaClass configured with no real url
    """
    token_uri = "mock://test_memoria.com/api-token-auth/"
    uri = "mock://test_memoria.com/api/QualityReportSession/3"
    expected_response = {"studies": ["a studie"]}
    with requests_mock.Mocker() as request_mock:
        request_mock.post(token_uri, json={"token": "test-token"})
        request_mock.get(uri, json=expected_response)
        assert expected_response == memoria_connector.get_report_session(3)


def test_post_experiment_calls_correct_endpoint(memoria_connector, request_with_token):
    """Test that on call post_experiment the correct service url is called

    Give: a memmoria conector
    When: calls post_experiment_run
    Then: the real post is maked to the correspondent url base on sessionType

    Args:
        memoria_connector (Memoria): fixture with a MemoriaClass configured with no real url
        request_with_token (Mock): Request mock that will be use on all the test code
    """
    quality_url = "mock://test_memoria.com/api/QualityReportExperimentRun/"
    training_url = "mock://test_memoria.com/api/TrainingExperimentRun/"
    request_with_token.post(
        training_url, json={"endpoint": training_url}, status_code=201
    )
    request_with_token.post(
        quality_url, json={"endpoint": quality_url}, status_code=201
    )
    t_response = memoria_connector.post_experiment_run(
        1, "test_name", "test_run", "info", FinishStatus.ok, SessionType.training
    )
    assert training_url == t_response["endpoint"]

    t_response = memoria_connector.post_experiment_run(
        1, "test_name", "test_run", "info", FinishStatus.ok, SessionType.qc
    )
    assert quality_url == t_response["endpoint"]
