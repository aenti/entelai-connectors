from enum import Enum


class FinishStatus(str, Enum):
    ok = "OK"
    error = "ERROR"


class SessionType(str, Enum):
    training = "TrainingSession"
    qc = "QualityControlSession"
