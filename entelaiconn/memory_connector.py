import requests
import logging

from entelaiconn.schemas import SessionType, FinishStatus
from typing import Dict, Any

JsonDict = Dict[str, Any]

loggin_level = logging.INFO
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=loggin_level
)


logger = logging.getLogger(__name__)


class MemoryConnector:
    """Class to connect to memory based on envirenment vars"""

    def __init__(self, base_url, user, password):
        """Initialize instance from enviorenment vars"""
        self.base_url = base_url
        self.user = user
        self.password = password

    def _get_token(self) -> str:
        """Make a token request

        Returns:
            str: token
        """
        url = f"{self.base_url}/api-token-auth/"

        payload = {"username": self.user, "password": self.password}

        response = requests.request("POST", url, data=payload)
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()["token"]

    def _get_auth_headers(self) -> Dict[str, str]:
        """Build a headers dict to make requests

        Returns:
            Dict: Dict with autorization headers
        """
        token = self._get_token()
        headers = {"Authorization": f"Token {token}"}
        return headers

    def get_report_session(self, session_identifier: int) -> JsonDict:
        """Returns a Report Session from Memory

        Args:
            session_identifier (int): session identifier to request

        Returns:
            JsonDict: [description]
        """
        headers = self._get_auth_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.base_url}/api/QualityReportSession/{session_identifier}"

        response = requests.request("GET", url, headers=headers, data={})
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def get_training_session(self, session_identifier: int) -> JsonDict:
        """Returns a Report Session from Memory

        Args:
            session_identifier (int): session identifier to request

        Returns:
            JsonDict: [description]
        """
        headers = self._get_auth_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.base_url}/api/TrainingSession/{session_identifier}"

        response = requests.request("GET", url, headers=headers, data={})
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def get_session(
        self, session_type: SessionType, session_identifier: int
    ) -> JsonDict:
        """Call Memory Training or QualityReport Session endpoint base on session_type

        Args:
            session_type (SessionType): Enum of sessions types
            session_identifier (int): session identifier

        Returns:
            JsonDict: [description]
        """
        if session_type == SessionType.training:
            return self.get_training_session(session_identifier)
        else:
            return self.get_report_session(session_identifier)

    def post_experiment_run(
        self,
        session_identifier: int,
        experiment_name: str,
        run_identifier: str,
        information: str,
        status: FinishStatus,
        session_type: SessionType,
    ):
        """Creates an experiment run on memory asociated with a session

        Args:
            session_identifier (int): Training or Quality session identifier
            experiment_name (str): experiment name (ideal the same as MLFlow)
            run_identifier (str): run identifier (idem)
            information (str): data on a string with information to save on memory
            status (FinishStatus): OR or ERROR status
            session_type (SessionType): training or quality session type
        """
        post_data = {
            "session": session_identifier,
            "name": experiment_name,
            "run_identifier": run_identifier,
            "info": information,
            "finish_status": status,
        }

        if session_type == SessionType.training:
            return self._post_training_experiment_run(post_data)
        else:
            return self._post_quality_experiment_run(post_data)

    def _post_quality_experiment_run(self, post_data: JsonDict):
        """Creates an experiment run on memory asociated with a quality session

        Args:
            post_data (dict): Dict with the data to post
        """

        headers = self._get_auth_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.base_url}/api/QualityReportExperimentRun/"

        response = requests.request("POST", url, headers=headers, json=post_data)
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def _post_training_experiment_run(self, post_data: JsonDict):
        """Creates an experiment run on memory asociated with a training session

        Args:
            post_data (dict): Dict with the data to post
        """

        headers = self._get_auth_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.base_url}/api/TrainingExperimentRun/"

        response = requests.request("POST", url, headers=headers, json=post_data)
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()
